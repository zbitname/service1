const moduleDependencies = {};

class B {
  static injectDependencies(...dependencies) {
    for (const dependency of dependencies) {
      moduleDependencies[dependency.name || dependency.constructor.name] = dependency;
    }
  }

  actionFromA() {
    new moduleDependencies.A().action();
  }

  action() {
    console.log('Action from service "B"');
  }
}

module.exports = B;
