const moduleDependencies = {};

class A {
  static injectDependencies(...dependencies) {
    for (const dependency of dependencies) {
      moduleDependencies[dependency.name || dependency.constructor.name] = dependency;
    }
  }

  actionFromB() {
    new moduleDependencies.B().action();
  }

  action() {
    console.log('Action from service "A"');
  }
}

module.exports = A;
