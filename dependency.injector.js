const A = require('./services/a');
const B = require('./services/b');

A.injectDependencies(B);
B.injectDependencies(A);
