require('./dependency.injector');

const A = require('./services/a');
const B = require('./services/b');

const a = new A();
const b = new B();

a.actionFromB();
b.actionFromA();
